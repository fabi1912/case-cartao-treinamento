package com.treinamento.cartoes.mapper;

import com.treinamento.cartoes.dto.ClienteRequest;
import com.treinamento.cartoes.dto.ClienteResponse;
import com.treinamento.cartoes.model.Cliente;

public class ClienteMapper {

    public  static Cliente fromClienteRequest(ClienteRequest clienteRequest){

        Cliente cliente = new Cliente();
        cliente.setName(clienteRequest.getName());
        return cliente;
    }

    public static ClienteResponse fromCliente(Cliente cliente){
        ClienteResponse clienteResponse = new ClienteResponse();
        clienteResponse.setName(cliente.getName());
        clienteResponse.setId(cliente.getId());
        return clienteResponse;
    }
}
