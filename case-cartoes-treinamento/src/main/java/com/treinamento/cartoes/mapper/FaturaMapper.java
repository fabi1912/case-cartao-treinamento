package com.treinamento.cartoes.mapper;

import com.treinamento.cartoes.dto.FaturaResponse;
import com.treinamento.cartoes.model.Fatura;

public class FaturaMapper {

    public static FaturaResponse fromFatura(Fatura fatura){
        FaturaResponse faturaResponse = new FaturaResponse();
        faturaResponse.setId(fatura.getId());
        faturaResponse.setPagoEm(fatura.getData());
        faturaResponse.setValorPago(fatura.getValor());
        return faturaResponse;
    }

}
