package com.treinamento.cartoes.mapper;

import com.treinamento.cartoes.dto.PagamentoRequest;
import com.treinamento.cartoes.dto.PagamentoResponse;
import com.treinamento.cartoes.model.Cartao;
import com.treinamento.cartoes.model.Pagamento;

import java.util.ArrayList;
import java.util.List;

public class PagamentoMapper {

    public static Pagamento fromPagamentoRequest(PagamentoRequest pagamentoRequest){
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(pagamentoRequest.getDescricao());
        pagamento.setValor(pagamentoRequest.getValor());
        Cartao cartao = new Cartao();
        cartao.setId(pagamentoRequest.getCartaoId());
        pagamento.setCartao(cartao);
        return pagamento;
    }

    public static PagamentoResponse fromPagamento(Pagamento pagamento){
        PagamentoResponse pagamentoResponse = new PagamentoResponse();
        pagamentoResponse.setId(pagamento.getId());
        pagamentoResponse.setCartaoId(pagamento.getCartao().getId());
        pagamentoResponse.setDescricao(pagamento.getDescricao());
        pagamentoResponse.setValor(pagamento.getValor());
        return pagamentoResponse;
    }

    public static List<PagamentoResponse> fromPagamentoList(List<Pagamento> pagamentos){
        List<PagamentoResponse> listaPagamento = new ArrayList<PagamentoResponse>();
        pagamentos.forEach(
                n-> listaPagamento.add(
                        new PagamentoResponse(n.getId(),n.getCartao().getId(),n.getDescricao(),n.getValor())));
        return listaPagamento;

    }

}
