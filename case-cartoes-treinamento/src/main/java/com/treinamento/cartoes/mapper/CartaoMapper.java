package com.treinamento.cartoes.mapper;

import com.treinamento.cartoes.dto.CartaoRequest;
import com.treinamento.cartoes.dto.CartaoResponse;
import com.treinamento.cartoes.dto.CartaoResponseResumido;
import com.treinamento.cartoes.model.Cartao;
import com.treinamento.cartoes.model.Cliente;

public class CartaoMapper{

    public static Cartao fromCartaoRequest(CartaoRequest cartaoRequest){
        Cartao cartao = new Cartao();
        Cliente cliente = new Cliente();
        cartao.setNumero(cartaoRequest.getNumero());
        cliente.setId(cartaoRequest.getClienteId());
        cartao.setAtivo(false);
        cartao.setCliente(cliente);
        return cartao;
    }

    public static CartaoResponse fromCartao(Cartao cartao){
        CartaoResponse cartaoResponse = new CartaoResponse();
        cartaoResponse.setAtivo(cartao.getAtivo());
        cartaoResponse.setClienteId(cartao.getCliente().getId());
        cartaoResponse.setId(cartao.getId());
        cartaoResponse.setNumero(cartao.getNumero());
        return cartaoResponse;
    }

    public static CartaoResponseResumido resumofromCartao(Cartao cartao){
        CartaoResponseResumido cartaoResponse = new CartaoResponseResumido();
        cartaoResponse.setClienteId(cartao.getCliente().getId());
        cartaoResponse.setId(cartao.getId());
        cartaoResponse.setNumero(cartao.getNumero());
        return cartaoResponse;
    }
}
