package com.treinamento.cartoes.repository;

import com.treinamento.cartoes.model.Fatura;
import org.springframework.data.repository.CrudRepository;

public interface FaturaRepository extends CrudRepository<Fatura,Long> {
}
