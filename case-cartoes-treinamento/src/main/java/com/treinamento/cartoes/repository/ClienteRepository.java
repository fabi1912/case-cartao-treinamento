package com.treinamento.cartoes.repository;

import com.treinamento.cartoes.model.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente,Long> {


}
