package com.treinamento.cartoes.repository;

import com.treinamento.cartoes.model.Pagamento;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento,Long> {

    String FIND_PAGAMENTO_BY_CARTAO =
            "SELECT * FROM pagamento pa"
                    + " WHERE pa.CARTAO_ID = :id_cartao";

    @Query(value=FIND_PAGAMENTO_BY_CARTAO, nativeQuery = true)
    List<Pagamento> recuperaPagamentos (@Param("id_cartao") Long idCartao);

    String DELETE_PAGAMENTO_BY_CARTAO =
            "DELETE FROM pagamento  WHERE cartao_id = :id_cartao";

    @Modifying
    @Transactional
    @Query(value=DELETE_PAGAMENTO_BY_CARTAO, nativeQuery = true)
    void removeLancamentos(@Param("id_cartao") Long idCartao);


}
