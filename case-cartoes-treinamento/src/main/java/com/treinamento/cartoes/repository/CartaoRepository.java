package com.treinamento.cartoes.repository;

import com.treinamento.cartoes.model.Cartao;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface CartaoRepository extends CrudRepository<Cartao,Long> {


    String FIND_CARTAO_BY_NUMERO =
            "SELECT * FROM cartao ca"
                    + " WHERE ca.CARTAO_NUMERO = :numero";

    @Query(value=FIND_CARTAO_BY_NUMERO, nativeQuery = true)
    Cartao buscaCartaoPeloNumero (@Param("numero") String numero);

}
