package com.treinamento.cartoes.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Fatura {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="FATURA_ID")
    private Long id;

    @Column(name="FATURA_DATA_PAGAMENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;

    @Column(name="FATURA_ID_CARTAO")
    private Long cartaoId;

    @Column(name="FATURA_VALOR")
    private Double valor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Long getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(Long cartaoId) {
        this.cartaoId = cartaoId;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
