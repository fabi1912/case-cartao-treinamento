package com.treinamento.cartoes.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Cliente {

    @Column(name="CLIENTE_NAME")
    @NotNull
    private String name;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="CLIENTE_ID")
    private Long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
