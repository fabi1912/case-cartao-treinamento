package com.treinamento.cartoes.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "Erro genérico ao processar pagamento")
public class PagamentoGenericException extends RuntimeException{

}
