package com.treinamento.cartoes.controller;

import com.treinamento.cartoes.dto.CartaoRequest;
import com.treinamento.cartoes.dto.CartaoResponse;
import com.treinamento.cartoes.dto.CartaoResponseResumido;
import com.treinamento.cartoes.dto.StatusCartaoRequest;
import com.treinamento.cartoes.exception.CartaoNotFoundException;
import com.treinamento.cartoes.exception.ClienteNotFoundException;
import com.treinamento.cartoes.mapper.CartaoMapper;
import com.treinamento.cartoes.model.Cartao;
import com.treinamento.cartoes.service.CartaoService;
import com.treinamento.cartoes.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    CartaoService cartaoService;

    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(code= HttpStatus.CREATED)
    public CartaoResponse insereCartao(@Valid @RequestBody CartaoRequest cartaoRequest) {

       if(clienteService.buscaCliente(cartaoRequest.getClienteId())!=null){
           return CartaoMapper.fromCartao(
                   cartaoService.insereCartao(
                           CartaoMapper.fromCartaoRequest(cartaoRequest)));
        }

        throw new ClienteNotFoundException();
    }

    @GetMapping("/{numero}")
    public CartaoResponseResumido buscaCliente(@Valid @PathVariable (value="numero") String numero) {

        Cartao cartao = cartaoService.buscaCartao(numero);
        if(cartao!=null){
            return CartaoMapper.resumofromCartao(cartao);
        }
        throw new CartaoNotFoundException();
    }

    @PatchMapping("/{numero}")
    public CartaoResponse atualizaStatusCartao(@Valid @PathVariable(value="numero") String numero , @RequestBody StatusCartaoRequest statusCartaoRequest) {

        Cartao cartao = cartaoService.atualizaCartao(statusCartaoRequest.getAtivo(), numero);
        if (cartao != null) {
            return CartaoMapper.fromCartao(cartao);
        }
        throw new CartaoNotFoundException();
    }
}
