package com.treinamento.cartoes.controller;

import com.treinamento.cartoes.dto.PagamentoRequest;
import com.treinamento.cartoes.dto.PagamentoResponse;
import com.treinamento.cartoes.exception.PagamentoGenericException;
import com.treinamento.cartoes.mapper.PagamentoMapper;
import com.treinamento.cartoes.service.CartaoService;
import com.treinamento.cartoes.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @Autowired
    CartaoService cartaoService;

    @ResponseStatus(code= HttpStatus.CREATED)
    @PostMapping
    public PagamentoResponse inserePagamento(@Valid @RequestBody PagamentoRequest pagamentoRequest){

        if(cartaoService.buscaPorId(pagamentoRequest.getCartaoId())!=null){
           return PagamentoMapper.fromPagamento(pagamentoService.inserePagamento(
                    PagamentoMapper.fromPagamentoRequest(pagamentoRequest)
            ));
        }
        throw new PagamentoGenericException();
    }

    @GetMapping("/{id_cartao}")
    public List<PagamentoResponse> buscaPagamentosCartao(@PathVariable(value="id_cartao") Long idCartao){

        if(cartaoService.buscaPorId(idCartao)!=null){
            return   PagamentoMapper.fromPagamentoList(
                    pagamentoService.recuperaPagamentosCartao(idCartao));
        }
        throw new PagamentoGenericException();

    }
}
