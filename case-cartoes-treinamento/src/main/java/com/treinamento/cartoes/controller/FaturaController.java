package com.treinamento.cartoes.controller;

import com.treinamento.cartoes.dto.BloqueioCartaoResponse;
import com.treinamento.cartoes.dto.FaturaResponse;
import com.treinamento.cartoes.dto.PagamentoResponse;
import com.treinamento.cartoes.exception.FaturaNotAuthorizated;
import com.treinamento.cartoes.mapper.CartaoMapper;
import com.treinamento.cartoes.mapper.FaturaMapper;
import com.treinamento.cartoes.mapper.PagamentoMapper;
import com.treinamento.cartoes.model.Cartao;
import com.treinamento.cartoes.model.Cliente;
import com.treinamento.cartoes.model.Pagamento;
import com.treinamento.cartoes.service.CartaoService;
import com.treinamento.cartoes.service.ClienteService;
import com.treinamento.cartoes.service.FaturaService;
import com.treinamento.cartoes.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/fatura")
public class FaturaController {

    @Autowired
    PagamentoService pagamentoService;

    @Autowired
    CartaoService cartaoService;

    @Autowired
    ClienteService clienteService;

    @Autowired
    FaturaService faturaService;

    @GetMapping("/{cliente-id}/{cartao-id}")
    public List<PagamentoResponse> recuperaFatura(@Valid @PathVariable(value="cliente-id") Long clienteId,
                                                  @PathVariable(value="cartao-id") Long cartaoId){

        Cliente cliente = clienteService.buscaCliente(clienteId);
        Cartao cartao = cartaoService.buscaPorId(cartaoId);

        if(cartao.getCliente().getId()!= cliente.getId()){
            throw new FaturaNotAuthorizated();
        }

        return PagamentoMapper.fromPagamentoList(pagamentoService.recuperaPagamentosCartao(cartaoId));
    }

    @PostMapping("/{cliente-id}/{cartao-id}/expirar")
    public BloqueioCartaoResponse expirarCartao(@Valid @PathVariable(value="cliente-id") Long clienteId,
                                                @PathVariable(value="cartao-id") Long cartaoId) {

        Cliente cliente = clienteService.buscaCliente(clienteId);
        Cartao cartao = cartaoService.buscaPorId(cartaoId);

        if(cartao.getCliente().getId()!= cliente.getId()){
            throw new FaturaNotAuthorizated();
        }

        cartaoService.atualizaCartao(false,cartao.getNumero());
        return new BloqueioCartaoResponse();
    }

    @PostMapping("/{cliente-id}/{cartao-id}/pagar")
    public FaturaResponse pagarFatura(@Valid @PathVariable(value="cliente-id") Long clienteId,
                                      @PathVariable(value="cartao-id") Long cartaoId) {

        Cliente cliente = clienteService.buscaCliente(clienteId);
        Cartao cartao = cartaoService.buscaPorId(cartaoId);

        if(cartao.getCliente().getId()!= cliente.getId()){
            throw new FaturaNotAuthorizated();
        }

        List<Pagamento> pagamentoList = pagamentoService.recuperaPagamentosCartao(cartaoId);
        FaturaResponse faturaResponse= FaturaMapper.fromFatura(faturaService.pagarFatura(pagamentoList,cartaoId));

        if(pagamentoList!=null && !pagamentoList.isEmpty()){
            pagamentoService.removePagamentos(cartaoId);
        }

        return faturaResponse;

    }

}
