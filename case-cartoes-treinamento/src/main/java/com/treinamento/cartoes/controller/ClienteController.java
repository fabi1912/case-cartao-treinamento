package com.treinamento.cartoes.controller;

import com.treinamento.cartoes.dto.ClienteRequest;
import com.treinamento.cartoes.dto.ClienteResponse;
import com.treinamento.cartoes.mapper.ClienteMapper;
import com.treinamento.cartoes.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

@PostMapping
@ResponseStatus(code= HttpStatus.CREATED)
public ClienteResponse insereCliente (@Valid @RequestBody ClienteRequest clienteRequest){

    return ClienteMapper.fromCliente(
            clienteService.insereCliente(
                    ClienteMapper.fromClienteRequest(clienteRequest)));
}

@GetMapping("/{id}")
public ClienteResponse buscaCliente(@Valid @PathVariable(value="id") Long id){
    return ClienteMapper.fromCliente(clienteService.buscaCliente(id));
}

}
