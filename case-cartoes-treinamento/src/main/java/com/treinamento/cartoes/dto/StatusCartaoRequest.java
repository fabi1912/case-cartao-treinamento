package com.treinamento.cartoes.dto;

import javax.validation.constraints.NotNull;

public class StatusCartaoRequest {

    @NotNull
    Boolean ativo;

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
