package com.treinamento.cartoes.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class ClienteRequest {

    @NotNull
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
