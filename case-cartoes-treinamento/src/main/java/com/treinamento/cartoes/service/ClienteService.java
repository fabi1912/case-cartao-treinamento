package com.treinamento.cartoes.service;

import com.treinamento.cartoes.exception.ClienteNotFoundException;
import com.treinamento.cartoes.model.Cliente;
import com.treinamento.cartoes.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    public Cliente insereCliente(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente buscaCliente(Long id){
        Optional<Cliente> cliente= clienteRepository.findById(id);
        if(cliente.isPresent()){
            return cliente.get();
        }
        throw new ClienteNotFoundException();
    }
}
