package com.treinamento.cartoes.service;

import com.treinamento.cartoes.exception.CartaoNotFoundException;
import com.treinamento.cartoes.model.Cartao;
import com.treinamento.cartoes.repository.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    CartaoRepository cartaoRepository;

    public Cartao insereCartao(Cartao cartao){
        return cartaoRepository.save(cartao);
    }

    public Cartao buscaCartao(String numero){
        return cartaoRepository.buscaCartaoPeloNumero(numero);
    }

    public Cartao buscaPorId(Long id){
        Optional<Cartao> cartao= cartaoRepository.findById(id);
        if(cartao.isPresent()){
            return cartao.get();
        }
        throw new CartaoNotFoundException();
    }

    public Cartao atualizaCartao(Boolean ativo, String numero){
        Cartao cartao ;

        cartao = cartaoRepository.buscaCartaoPeloNumero(numero);
        if(cartao!=null) {
            cartao.setAtivo(ativo);
            cartaoRepository.save(cartao);
        }
        return cartao;
    }

}
