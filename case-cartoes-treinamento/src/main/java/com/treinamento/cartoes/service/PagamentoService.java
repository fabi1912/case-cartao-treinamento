package com.treinamento.cartoes.service;

import com.treinamento.cartoes.model.Pagamento;
import com.treinamento.cartoes.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;

    public Pagamento inserePagamento(Pagamento pagamento){
        return pagamentoRepository.save(pagamento);
    }

    public List<Pagamento> recuperaPagamentosCartao(Long idCartao){
        return (List<Pagamento>) pagamentoRepository.recuperaPagamentos(idCartao);
    }

    public void removePagamentos(Long cartaoId){
        pagamentoRepository.removeLancamentos(cartaoId);
    }

}
