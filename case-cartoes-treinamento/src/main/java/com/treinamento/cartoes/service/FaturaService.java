package com.treinamento.cartoes.service;

import com.treinamento.cartoes.dto.PagamentoResponse;
import com.treinamento.cartoes.model.Fatura;
import com.treinamento.cartoes.model.Pagamento;
import com.treinamento.cartoes.repository.FaturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class FaturaService {

    @Autowired
    FaturaRepository faturaRepository;


    public  Fatura pagarFatura(List<Pagamento> pagamentoList , Long cartaoId){
        Fatura fatura = new Fatura();
        fatura.setData(new Date());
        fatura.setCartaoId(cartaoId);
        double valorTotal=0;

        if(pagamentoList!=null && !pagamentoList.isEmpty()){
            for(Pagamento pagamento:pagamentoList){
                valorTotal = valorTotal + pagamento.getValor();
            }
        }

        fatura.setValor(valorTotal);
        return faturaRepository.save(fatura);
    }


}
